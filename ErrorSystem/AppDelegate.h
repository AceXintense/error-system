//
//  AppDelegate.h
//  Error System
//
//  Created by Sam Grew on Thursday, 14 July 2016
//  Copyright (c) Sam Grew. All rights reserved.
//

#import <UIKit/UIKit.h>

@class StandaloneCodeaViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) StandaloneCodeaViewController *viewController;

@end
