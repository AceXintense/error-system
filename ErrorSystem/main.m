//
//  main.m
//  Error System
//
//  Created by Sam Grew on Thursday, 14 July 2016
//  Copyright (c) Sam Grew. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
