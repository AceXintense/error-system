-- Error System

-- Use this function to perform your initial setup
function setup()
    version = 0.1
    
    if version < 1 then
        flash = Flash('Version ' .. version,'Warning')
    end
    
    button = Button(10, 10, 100, 50, color(50, 50, 240))
end

-- This function gets called once every frame
function draw()
    -- This sets a dark background color 
    background(50, 50, 50, 255)
    --displayMode(FULLSCREEN)

    -- This sets the line thickness
    strokeWidth(5)

    -- Do your drawing here

    if flash then
        flash:draw()
    end
        
    if button then
        button:draw()
    end
end

function touched(touch)
    if touch.state == BEGAN then
        button:touched()
    end
end

