Button = class()

function Button:init(x, y, width, height, ccolor)
    -- you can accept and set parameters here
    self.x = x
    self.y = y
    self.width = width
    self.height = height
    self.color = ccolor
    self.toggled = false
end

function Button:draw()
    -- Codea does not automatically call this method
    noStroke()
    fill(self.color)
    rect(self.x, self.y, self.width, self.height)
end

function Button:touched(touch)
    print(self.toggled)
end
